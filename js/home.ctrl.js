app.controller('MainCtrl', function($scope, $location, $mdDialog, $mdToast, $rootScope, $routeParams, $http, $window, $log, $document, $mdSidenav, $timeout, fileUpload) {
  $scope.isPath= function(viewLocation) {
    return viewLocation === $location.path();
  };
  $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

  function buildToggler(componentId) {
    return function() {
      $mdSidenav(componentId).toggle();
    };
  }
  $scope.cancel=function() {
    $mdDialog.cancel();
  }
  $timeout(function() {
    $scope.isLoading=true;
  }, 500);


  $scope.submissionPopup=function (ev) {
    var confirm = $mdDialog.prompt()
      .title('Admin credentials')
      .textContent('please insert password')
      .placeholder('password')
      .ariaLabel('password')
      .initialValue('')
      .targetEvent(ev)
      .ok('Ok')
      .cancel('Cancel');
    $mdDialog.show(confirm).then(function(result) {
      console.log(result);
      var fd = new FormData();
      uploadUrl='php/getsubmission.php'
      fd.append('password', result);
      $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'Process-Data': false}
      }).then(function successCallback(response) {
        $scope.submissionList=response.data;
        $location.path('/getsubmission');
      }, function errorCallback(error) {
      });
    }, function() {
      $scope.status = 'You didn\'t enter the password';
    });
  };
  $scope.register = function(ev) {
    $mdDialog.show({
      controller:'MainCtrl',
      templateUrl: 'templates/register.popup.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
      }, function() {
      });
  };
 	$scope.submit = function(up){
	  var file = $scope.uploadedFile;
    // console.log(up);
    // console.log(file);
    $scope.uploadingTrue=true;
    if($scope.uploadedFile==null){
      $mdToast.show(
        $mdToast.simple()
          .textContent('Please attach file first')
          .hideDelay(5000)
          .position('right bottom')
        );
    }
    else{
      var uploadUrl = "php/upload.php";
      var data=up;
      fileUpload.uploadFileToUrl(file, uploadUrl, data);
    }
 	};

  $scope.carouselImages= [
       {
            "title" : "Latest Technology",
            "img" : "images/image2-min.JPG",
            "description":"latest developments in technology and engineering are shown"
        },{
            "title" : "Learning",
            "img" : "images/image5-min.JPG",
            "description":"An excellent platform to learn new things"
        },{
            "title" : "Intellectual Hub",
            "img" : "images/image9-min.JPG",
            "description":"An intellectual hub where you can interact with students from various institutes"
        }
    ];
  $scope.imagePath = "images/images1.jpg";
  $scope.attractions=[
    {
      "title":"Talk by Subrat Kar",
      "description":"Professor Subrat Kar is the professor of electrical engineering department in IIT Delhi."+
      "Join an amazing session with him to get a general idea of engineering.",
      "datetime":""
    },
    {
      "title":"Painting/Drawing Competition on Cyber Security Awareness",
      "description":"Centre of Excellence for Cyber Systems and Information Assurance ,IIT Delhi invites students from all the recognized State/Central"+
      "Government schools (State/CBSE/ICSE/NCRT) across the NCR region to participate in Painting & Drawing competition.",
      "datetime":""
    },
    {
      "title":"On the Spot Energy Quiz Competition",
      "description":"It is a quiz competition organized by Energy Forum (CES, IIT Delhi) for school children in Open House i.e. on 23rd April 2016. The quiz will test your general "+
      "knowledge in the field of Energy and Environment. It aims to create awareness in the field of energy.",
      "datetime":""
    }
  ];


  $scope.posterdata = [
  {"uid":"2","filename":"ewrwe.png","name":"Amit Kumar","title":"Two stroke engine: Past and Future","email":"kumaramit14346@gmail.com","type":"image/png","size":"10132","snumber":"9911691214","created":"2017-04-13 23:30:43"},
  {"uid":"3","filename":"ewrwe.png","name":"Amit Kumar","title":"Two stroke engine: Past and Future","email":"kumaramit14346@gmail.com","type":"image/png","size":"10132","snumber":"9911691214","created":"2017-04-13 23:30:45"},
  {"uid":"4","filename":"ewrwe.png","name":"Amit Kumar","title":"Two stroke engine: Past and Future","email":"kumaramit14346@gmail.com","type":"image/png","size":"10132","snumber":"9911691214","created":"2017-04-13 23:30:47"},
  {"uid":"5","filename":"ewrwe.png","name":"Amit Kumar","title":"Two stroke engine: Past and Future","email":"kumaramit14346@gmail.com","type":"image/png","size":"10132","snumber":"9911691214","created":"2017-04-13 23:30:49"},
  {"uid":"6","filename":"ewrwe.png","name":"Amit Kumar","title":"Two stroke engine: Past and Future","email":"kumaramit14346@gmail.com","type":"image/png","size":"10132","snumber":"9911691214","created":"2017-04-13 23:39:33"},
  {"uid":"7","filename":"ewrwe.png","name":"Amit Kumar","title":"Two stroke engine: Past and Future","email":"kumaramit14346@gmail.com","type":"image/png","size":"10132","snumber":"9911691214","created":"2017-04-13 23:40:01"}
];



  $scope.coordinators={
    "Overall Cordinator":[
        {
          "name":"Deepak Korku",
          "img":"images/korku.jpg",
          "email":"korkudeepak@gmail.com",
          "contact":"9109211457"
        },

      ],

    "Website Coordinators":[
        {
          "name":"Sangam Bharti",
          "img":"images/sangam.jpg",
          "email":"sangamsam96@gmail.com",
          "contact":"9560933374"
        },
        {
          "name":"Nikhil Meena",
          "img":"images/nikhil.jpg",
          "email":" ",
          "contact":" "
        }
      ],
      "App Coordinators":[
          {
            "name":"Sachin Kumawat",
            "img":"images/sachin.jpg",
            "email":"sachinkumawat2206@gmail.com",
            "contact":"9560942206"
          }
        ]
    };
  // console.log($scope.coordinators);

  $scope.facultycoordinators={
    "Chairman":[
        {
          "name":"Mr. B K Panigrahi",
          "img":"images/panigrahi.jpg",
          "email":"bkpanigrahi@ee.iitd.ac.in"
        }
      ],
      "Vice-Chairman":[
          {
            "name":"Mr. Anil Verma",
            "img":"images/anil sir.jpg",
            "email":"anilverma@chemical.iitd.ac.in"
          },
          {
            "name":"Mr. Jayant Jain",
            "img":"images/jayant jain.png",
            "email":"jayantj@am.iitd.ac.in"
          },
          {
            "name":"Mr. Nidhi Jain",
            "img":"images/nidhi.jpg",
            "email":"njain@chemistry.iitd.ac.in"
          }
        ],
  "Invitation and Registration":[
      {
        "name":"Jayant Jain",
        "img":"images/jayant jain.png",
        "email":"jayantj@am.iitd.ac.in"
      },
      {
        "name":"Shuchi Sinha",
        "img":"images/Shuchi-Sinha.jpg",
        "email":"shuchi@dms.iitd.ac.in"
      },
      {
        "name":"Bijaya P Tripathy",
        "img":"images/Bijaya P Tripathy.jpg",
        "email":"btripathi@polymers.iitd.ac.in"
      },
      {
        "name":"Deepak kumar ",
        "img":"images/Deepak Kumar.gif",
        "email":"dkumar@itmmec@iitd.ac.in"
      }

    ],
  "Infrastructure":[
    {
      "name":"Jayant Jain",
      "img":"images/jayant jain.png",
      "email":"jayantj@am.iitd.ac.in"
    },
    {
      "name":"Shravan Kumar ",
      "img":"images/Shravan Kumar.jpg",
      "email":"shravankumar@maths.iitd.ac.in"
    },
    {
      "name":"Nezamuddin",
      "img":"images/nezamudin.jpg",
      "email":"nezam@civil.iitd.ac.in"
    },
    {
      "name":"Bhasker Kanseri",
      "img":"images/Bhaskar Kanseri 1.jpg",
      "email":"bkanseri@physics.iitd.ac.in"
    }
    ],
    "Food and Catering":[
      {
        "name":"Anil Verma",
        "img":"images/anil sir.jpg",
        "email":"anilverma@chemical.iitd.ac.in"
      },
      {
        "name":"Hariprasad Kodamna",
        "img":"images/Hariprasad Kodamana.jpg",
        "email":"kodamana@chemical.iitd.ac.in"
      },
      {
        "name":"Sandeep Jha",
        "img":"images/sandeep.jpg",
        "email":"sandeepjha@cbme.iitd.ac.in"
      }
      ],
      "Media":[
        {
          "name":"Anil Verma",
          "img":"images/anil sir.jpg",
          "email":"anilverma@chemical.iitd.ac.in"

        },
        {
          "name":"Javed Nabibaksha S",
          "img":"images/Javed Nabibaksha S.jpg",
          "email":"jnsheikh@textile.iitd.ac.in"
        },
        {
          "name":"Satyananda Kar",
          "img":"images/Satyananda_Kar.jpg",
          "email":"satyananda@ces.iitd.ac.in"
        },
        {
          "name":"Mahesh Abegaonkar",
          "img":"images/mahesh_abegaonkar.jpg",
          "email":"dtanmay@chemistry.iitd.ac.in"
        }
        ],
        "Industry Session":[
          {
            "name":"Vasant Matsagar",
            "img":"images/person.png",
            "email":"matsagar@civil.iitd.ac.in"
          },
          {
            "name":"S.S. Bahga",
            "img":"images/person.png",
            "email":"   "
          },
          {
            "name":"Tanmay Dutta",
            "img":"images/person.png",
            "email":"  "
          }
        ],
        "Industry Session":[
          {
            "name":"Mahesh Abegaonkar",
            "img":"images/mahesh_abegaonkar.jpg",
            "email":"dtanmay@chemistry.iitd.ac.in"
          },
          {
            "name":"Shalini Gupta",
            "img":"images/shalini maam 2.jpg",
            "email":"shalinig@chemical.iitd.ac.in"
          },
          {
            "name":"Sheshan Rangarajan",
            "img":"images/Seshan Srirangarajan.JPG",
            "email":"seshan@ee.iitd.ac.in"
          }
        ],
        "Web and App":[
          {
            "name":"Anil Verma",
            "img":"images/anil sir.jpg",
            "email":""
          },
          {
            "name":"Rijurekha Sen",
            "img":"images/Rijurekha Sen.JPG",
            "email":"riju@cse.iitd.ac.in"
          },
          {
            "name":"Deepak U.Patil",
            "img":"images/Deepak U.Patil.jpg",
            "email":"deepakpatil@ee.iitd.ac.in"
          },
          {
            "name":"Seshan Srirangarajan",
            "img":"images/Seshan Srirangarajan.JPG",
            "email":"seshan@ee.iitd.ac.in"
          }
        ],
        "Print and Design":[
          {
            "name":"Nidhi Jain",
            "img":"images/nidhi.jpg",
            "email":"njain@chemistry.iitd.ac.in"
          },
          {
            "name":"Sumer Singh",
            "img":"images/sumer.JPG",
            "email":"sumerid@iddc.iitd.ac.in"
          },
          {
            "name":"Ankur Gupta",
            "img":"images/Ankur Gupta.jpg",
            "email":"ankurgupta@care.iitd.ac.in"
          }
        ],
        "Hospitality and Reception":[
          {
            "name":"Nidhi Jain",
            "img":"images/nidhi.jpg",
            "email":"njain@chemistry.iitd.ac.in"
          },
          {
            "name":"Shaikh Ziauddin Ahar",
            "img":"images/Shaikh Ziauddin Ahar.jpg",
            "email":"zia@dbeb.iitd.ac.in"
          },
          {
            "name":"Sayantan Paria",
            "img":"images/paria.JPG",
            "email":"sparia@chemistry.iitd.ac.in"
          }
        ],
        "Inauguration and Awards":[
          {
            "name":"Nidhi Jain",
            "img":"images/nidhi.jpg",
            "email":"njain@chemistry.iitd.ac.in"
          },
          {
            "name":"Arjun Sharma",
            "img":"images/Arjun Sharma.jpg",
            "email":"arjunsharma@am.iitd.ac.in"
          },
          {
            "name":"J.P.Khatait",
            "img":"images/J P Khatait.jpg",
            "email":"jpkhatait@mech.iitd.ac.in"
          },
          {
            "name":"Kavya Dashora",
            "img":"images/Kavya Dashora.jpg",
            "email":"kdashora@rdat.iitd.ac.in"
          }
        ]

  };
// console.log($scope.facultycoordinators);
$scope.photoGallery = [
                {
                  "name":"",
                  "image":"images/1-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/2-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/3-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/4-min.JPG"
                },
                // {
                //   "name":"",
                //   "image":"images/5-min.JPG"
                // },
                {
                  "name":"",
                  "image":"images/6-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/7-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/8-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/9-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/10-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/11-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/12-min.JPG"
                },
                // {
                //   "name":"",
                //   "image":"images/13-min.JPG"
                // },
                {
                  "name":"",
                  "image":"images/14-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/15-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/16-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/17-min.JPG"
                },
                {
                  "name":"",
                  "image":"images/18-min.JPG"
                }
];
$scope.enumpeople = [];
$scope.peoplenames = [];
$scope.addpeople = function (data) {
  $scope.enumpeople = [];
  $scope.peoplenames = [];
  console.log(data);
  for(var i=0; i<data; i++){
    $scope.enumpeople.push(' ');
  }
}

$scope.addpeoplenames = function(data){
  $scope.peoplenames.push(data);
  console.log($scope.peoplenames);
}

$scope.isRegloading=false;

$scope.submitReg=function (user, reg_name) {

  console.log(user);
  $scope.isRegloading=true;

  $http({
    url:"http://openhouse.iitd.ac.in/php/submitforms.php?reg_name="+reg_name,
    method:"POST",
    // headers:{
    //   'Content-Type': 'application/json; charset=UTF-8'

    // },
    data:{
          'enum_people':user.enumpeeps,
          'address':user.address,
          'org_name':user.org_name,
          'contact_person':user.contact_person,
          'people_name':$scope.peoplenames,
          'team_leader':user.team_leader,
          'email':user.email,
          'phone':user.number
      }
  }).then(function sucessCallback(response) {
    $mdDialog.show({
    controller:'MainCtrl',
    templateUrl: 'templates/toast.html',
    parent: angular.element(document.body),
    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
    }, function() {
    });
      $scope.isRegloading=false;
      //$scope.user = null;
      //$scope.enumpeople = [];
      //$scope.peoplenames = [];
  }, function errorCallback(error) {
      console.log(error);
      $mdToast.show(
        $mdToast.simple()
        .textContent(error.data.message)
        .position('bottom right')
        .hideDelay(3000)
      );
      $scope.isRegloading=false;
  });


}

$scope.checkAdmin=function(admin){
  $http({
    url:"",
    method:"POST",
    data:{
      'user_name':admin.username,
      'password':admin.pass
    }
  }).then(function successCallback(response){},function errorCallback(error){
    admin.username='';
    admin.pass='';
    $mdDialog.show({
    controller:'MainCtrl',
    templateUrl: 'templates/toasterr.html',
    parent: angular.element(document.body),
    clickOutsideToClose:true,
    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
    }, function() {
    });
  });
}


});
