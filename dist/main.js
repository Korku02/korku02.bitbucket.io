
var app = angular.module('app', ['ngMaterial','ngAnimate','ngRoute','ngMessages','angular-carousel']);
app.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
  .primaryPalette('teal', {
   'default': '400', // by default use shade 400 from the pink palette for primary intentions
   'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
   'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
   'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
  })
  .accentPalette('orange');
});
app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
  $routeProvider.when("/", {
    controller: "MainCtrl",
    templateUrl: "templates/home.html"
  }).when("/registration", {
    controller: "MainCtrl",
    templateUrl: "templates/registration.html"
  }).when("/attractions", {
    controller: "MainCtrl",
    templateUrl: "templates/comingsoon.html"
  }).when("/getsubmission", {
    controller: "MainCtrl",
    templateUrl: "templates/comingsoon.html"
  }).when("/student", {
    controller: "MainCtrl",
    templateUrl: "templates/students.html"
  }).when("/faculty", {
    controller: "MainCtrl",
    templateUrl: "templates/faculty.html"
  }).when("/schedule", {
    controller: "MainCtrl",
    templateUrl: "templates/schedule.html"
  }).when("/submission", {
    controller: "MainCtrl",
    templateUrl: "templates/comingsoon.html"
  }).when("/demos", {
    controller: "MainCtrl",
    templateUrl: "templates/demos.html"
  }).when("/visitor", {
    controller: "MainCtrl",
    templateUrl: "templates/visitor.html"
  }).when("/startups", {
    controller: "MainCtrl",
    templateUrl: "templates/visitor.html"
  }).when("/project", {
    controller: "MainCtrl",
    templateUrl: "templates/visitor.html"
  }).when("/professionals", {
    controller: "MainCtrl",
    templateUrl: "templates/visitor.html"
  }).when("/downloads", {
    controller: "MainCtrl",
    templateUrl: "templates/comingsoon.html"
  }).when("/adminlogin",{
    controller: "MainCtrl",
    templateUrl: "templates/adminlogin.html"
  }).otherwise({
    controller: "MainCtrl",
    templateUrl: "templates/error.html"
  });
}]);
app.directive('fileModel', ['$parse', function ($parse) {
    return {
    restrict: 'A',
    link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
    }
   };
}]);
app.service('fileUpload', ['$http','$mdDialog','$mdToast','$rootScope', function ($http,$mdDialog,$mdToast,$rootScope) {
    this.uploadFileToUrl = function(file, uploadUrl, data){
         var fd = new FormData();
         fd.append('file', file);
         fd.append('size', file.size);
         fd.append('type', file.type);
         fd.append('title', data.title);
         fd.append('name', data.name);
         fd.append('number', data.number);
         fd.append('email', data.email);
         $http.post(uploadUrl, fd, {
             transformRequest: angular.identity,
             headers: {'Content-Type': undefined,'Process-Data': false}
         }).then(function successCallback(response) {
           $mdToast.show(
             $mdToast.simple()
               .textContent(response.data)
               .hideDelay(5000)
               .position('right bottom')
             );
           $rootScope.isLoading=true;
         }, function errorCallback(error) {
           console.log(error);
         });
     }
 }]);
