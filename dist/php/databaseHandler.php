<?php


	class DB_Connection{
		private $DB_NAME = "openhouse";
		private $USERNAME = "root";
		private $PASSWORD = "3276";
		private $SERVER_NAME = "localhost";

		public function connect(){
			$conn = new mysqli($this->SERVER_NAME, $this->USERNAME, $this->PASSWORD, $this->DB_NAME);
			return $conn;
		}
	}

	class DB_Handler{
		private $db = null;
		public function __construct(){
			$this->db = new DB_Connection();
		}
		public function save($filename, $title , $name, $email, $type, $size, $number){
			$conn = $this->db->connect(); // create a connection to database.
			$query = "INSERT INTO submission (title, filename, name, email, type, size, snumber) VALUES ('".$title."','".$filename."','".$name."','".$email."','".$type."','".$size."','".$number."')"; // create the query.
			$result = $conn->query($query) or die($conn->error.__LINE__);
			// close the connection
			$conn->close();
			return $result;
		}
		public function get(){
			$conn = $this->db->connect(); // create a connection to database.
			$query = "SELECT * FROM submission"; // create the query.
			$result = $conn->query($query) or die($conn->error.__LINE__);
			$filedata = array();
			if($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					$filedata[] = $row;
				}
			}
			// close the connection
			$conn->close();
			return $filedata;
		}
	}

?>
