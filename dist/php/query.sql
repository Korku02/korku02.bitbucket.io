CREATE DATABASE IF NOT EXISTS openhouse;

USE openhouse;

CREATE TABLE IF NOT EXISTS `submission` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `filename` text NOT NULL,
  `name` text NOT NULL,
  `title` text NOT NULL,
  `email` text NOT NULL,
  `type` text NOT NULL,
  `size` int(11) NOT NULL,
  `snumber` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
