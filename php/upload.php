<?php
  include("databaseHandler.php");
  $allowedExts = array("jpg", "jpeg", "pdf", "png");
  $temp = explode(".", $_FILES["file"]["name"]);
  $extension = end($temp);
  if ((($_FILES["file"]["type"] == "application/pdf")
  || ($_FILES["file"]["type"] == "image/pjpeg")
  || ($_FILES["file"]["type"] == "image/png")
  || ($_FILES["file"]["type"] == "image/jpeg"))
  && ($_FILES["file"]["size"] < 10485760)
  && in_array($extension, $allowedExts))
  {
    $title = $_POST['title'];
    $thumbnail = $_POST['size'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $type = $_POST['type'];
    $number = $_POST['number'];
    $size = $_POST['size'];
    if (!empty( $_FILES ) ) {
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
        $uploadPath = "submission/".$_FILES[ 'file' ][ 'name' ];
      if(move_uploaded_file($tempPath, $uploadPath)){
        $db = new DB_Handler();
        $result = $db->save($_FILES[ 'file' ][ 'name' ],$title, $name, $email, $type, $size, $number);
        unset($db);
        if($result){
          echo "Uploaded Successfully!";
        }else{
          echo "Uploaded but not saved!";
        }
      }else{
          echo "Failed to upload!";
      }
    }
  }
  else {
    echo "Error occurred!";
  }
?>
